MT:=
CXX:=/opt/rh/devtoolset-2/root/usr/bin/g++
BASE_DIR:=/home/ysriram/projects/qurl-svc
VENDOR_DIR:=/usr/local/vendor-libs/install
ADDITIONAL_LD_FLAGS := -Wl,-rpath,$(VENDOR_DIR)/lib
#WARN_ALL := -Wall
WARN_ALL:=

INC := -I$(VENDOR_DIR)/include \
	-I$(BASE_DIR) \
	-I$(BASE_DIR)/include

ALL_SLIBS_PATH := \
	-L$(VENDOR_DIR)/lib

ALL_SLIBS := \
	-lboost_regex$(MT) \
	-lboost_system$(MT) \
	-lboost_date_time$(MT) \
	-lboost_thread$(MT) \
	-lboost_filesystem$(MT) \
	-lboost_atomic$(MT) \
	-lboost_timer$(MT) \
	-lhiredis \
	-lonioncpp \
	-lonion \
	-lcurl \
	-lpthread \
	-ldl


QURL_SVC_SRC := \
	$(BASE_DIR)/json11/json11.cpp \
	src/common.cpp \
	src/logutils.cpp \
	src/sysutils.cpp \
	src/backend.cpp \
	src/svc-manager.cpp \
	src/svc-handler.cpp \
	src/svc-main.cpp

QURL_TEST_SVC_SRC := \
	$(BASE_DIR)/json11/json11.cpp \
	src/common.cpp \
	src/logutils.cpp \
	src/sysutils.cpp \
	src/backend.cpp \
	src/svc-manager.cpp \
	src/svc-handler.cpp \
	test/lifecycle.cpp

QURL_SVC_OBJ := $(patsubst %.cpp,%.o, $(QURL_SVC_SRC))
QURL_TEST_SVC_OBJ := $(patsubst %.cpp,%.o, $(QURL_TEST_SVC_SRC))


.PHONY: all clean

all: qurl-svc


.cpp.o: %.cpp
	$(CXX) -std=c++11 -fPIC -fpermissive  -g $(WARN_ALL) -DHAVE_INTTYPES_H -DHAVE_NETINET_IN_H $(INC) -c $< -o $@


all: qurl-svc test

qurl-svc: $(QURL_SVC_OBJ)
	$(CXX) $^ -o bin/$@ $(ADDITIONAL_LD_FLAGS) $(ALL_SLIBS_PATH) $(ALL_SLIBS)

test: $(QURL_TEST_SVC_OBJ)
	$(CXX) $^ -o bin/$@ $(ADDITIONAL_LD_FLAGS) $(ALL_SLIBS_PATH) $(ALL_SLIBS) -lboost_unit_test_framework

clean:
	$(RM) $(SVC_DEPS) src/*.o test/*.o bin/*


