# qurl-svc #

A simple queue service with REST API to manage jobs. The current implementation is backed by `redis`.

# API #

## POST /jobs ##

Will create a new job and issue a `job_id`. The job status is currentl `OPEN`.

_Request data_:

```
{
 "queue": "example:queue",
 "anything_else": ...
}
```

_Response data_:

```
{
 "id": "...",
 "status": "OK|NOK"
}
```


## GET /jobs ##

Will pop a job from the queue. The job status is moved to `TAKEN`.

_Response data_:

```
{
  "id": "..",
  "job_status": "TAKEN",
  "created_at": "YYYY/mm/dd HH:MM:SS",
  "payload": {
      "your payload"
  },
 "status": "OK|NOK"
}
```

## PUT /jobs ##

This is to acknowledge that you are working on the job.

_Request params_:
```
q = queue name
id = job id
```

_Response data_:

```
{
  "status": "OK|NOK"
}
```


## DELETE /jobs ##

This is to mark the job as `DONE`

_Request params_:
```
q = queue name
id = job id
```

_Response data_:

```
{
  "status": "OK|NOK"
}
```

# Configuration #

```
{
  "instance": "qurlsvc-1",
  "home_dir": "<path>/qurl-svc",
  "log_dir": "<path>/log",
  "log_file": "qurl-svc.log",
  "log_level": "info",
  "pid_file": "<path>/log/qurl-svc.pid",
  "port": 3000,
  "daemonize": true,

  "maint_timer_milli": 3000,
  "max_threads": 5,

  "redis": {
    "host": "192.168.1.45",
    "port": 6379,
    "connect_timeout_sec": 3
  }
}
```

# Running The Service #

The service will create a `log` file in the directory specified in config, and 
also a `pid` file as configured.

_Start_:

```
$ <path>/bin/qurl-svc --config <path>/config.json
```

_Stop_:

```
$ kill <pid>
```

# Tests #

```
$ <path>/bin/test  --log_level=all

Running 1 test case...
Entering test suite "LIFECYCLE"
Entering test case "lifecycle_simple"
test/lifecycle.cpp(58): info: check 'initiate job ...' passed
test/lifecycle.cpp(59): info: check 'not empty id ...' passed
test/lifecycle.cpp(62): info: check 'take job ...' passed
test/lifecycle.cpp(63): info: check 'take job status ...' passed
test/lifecycle.cpp(65): info: check 'ack job ...' passed
test/lifecycle.cpp(68): info: check 'ack job in wrong state ...' passed
test/lifecycle.cpp(71): info: check 'done job ...' passed
test/lifecycle.cpp(74): info: check 'done job not exist ...' passed
test/lifecycle.cpp(78): info: check 'take job empty ...' passed
test/lifecycle.cpp(79): info: check 'take job empty id must be nil...' passed
Leaving test case "lifecycle_simple"; testing time: 10ms
Leaving test suite "LIFECYCLE"

```

# TODO #

- Document dependencies

- Fix issue in `libonion` PUT .. some reason it chokes. For not it's `POST` where we check if `id` is present.
