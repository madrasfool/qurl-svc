#ifndef __BACKEND__H_
#define __BACKEND__H_

#include <json11/json11.hpp>

#include "common.h"

using namespace json11;

namespace qurlsvc {

  class backend {
  protected:
    const Json &config;
  public:
    backend(const Json& config_):config(config_) {}
    virtual ~backend(){}

    virtual svc_status initiate(const std::string& queue,const Json &job, job_id& id)=0;
    virtual svc_status take(const std::string& queue, Json &job,const uint32_t timeout=0)=0;
    virtual svc_status ack(const std::string& queue, const job_id& id)=0;
    virtual svc_status done(const std::string& queue, const job_id& id)=0;

    
    static std::shared_ptr<backend> make(const Json& config);
  };

}

#endif
