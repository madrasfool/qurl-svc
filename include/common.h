#ifndef __COMMON_H__
#define __COMMON_H__

#include <string>
#include "boost/date_time/posix_time/posix_time.hpp"

namespace qurlsvc {

  std::string utc_now(const std::string& fmt,boost::posix_time::ptime now_utc);
  std::string utc_at(const std::string& fmt,const uint32_t days);

  typedef std::string job_id;
  enum class svc_status{OK=0,NOK=1};
  enum class job_status{OPEN=0,TAKEN=1,ACKD=2,DONE=3,ERROR=4};
}

#endif
