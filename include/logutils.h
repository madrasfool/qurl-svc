#ifndef __LOGUTILS__
#define __LOGUTILS__

namespace qurlsvc {
  class Log {
  public:
    static void close();
    static void error(const std::string &msg);
    static void warn(const std::string &msg);
    static void info(const std::string &msg);
    static void debug(const std::string &msg);
    static void admin(const std::string &msg);
  };
};

#endif
