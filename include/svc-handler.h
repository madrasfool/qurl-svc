#ifndef __SVC_HANDLER_H__
#define __SVC_HANDLER_H__

#include <onion/request.hpp>
#include <onion/response.hpp>

#include "svc-manager.h"

namespace qurlsvc {

  class svc_handler {
  protected:
    const Json& config;
    std::shared_ptr<svc_manager_base> mgr;
  public:
    svc_handler(const Json& config_,std::shared_ptr<svc_manager_base>mgr_):config(config_),mgr(mgr_){}
    virtual ~svc_handler(){}
    virtual onion_connection_status status(Onion::Request &req, Onion::Response &res);
    virtual onion_connection_status jobs(Onion::Request &req, Onion::Response &res);

  protected:    
    onion_connection_status initiate(Onion::Request &req, Onion::Response &res);
    onion_connection_status take(Onion::Request &req, Onion::Response &res);
    onion_connection_status ack(Onion::Request &req, Onion::Response &res);
    onion_connection_status done(Onion::Request &req, Onion::Response &res);

  protected:
    void dump_cors_headers(Onion::Response &res);
    bool is_options(Onion::Request &req);
    bool is_get(Onion::Request &req);
    bool is_post(Onion::Request &req);
    bool is_put(Onion::Request &req);
    bool is_delete(Onion::Request &req);
    svc_status json_payload(Onion::Request &req, Json& payload);
  };
}

#endif
