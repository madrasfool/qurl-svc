#ifndef __SVC_MANAGER_H__
#define __SVC_MANAGER_H__

#include <boost/asio.hpp>
#include <boost/timer.hpp>
#include <json11/json11.hpp>

#include "common.h"
#include "backend.h"

using namespace json11;

namespace qurlsvc {

  class svc_manager_base {
  protected:
    const Json& config;
    std::shared_ptr<backend> store;
  public:
    svc_manager_base(const Json& config
		     ,std::shared_ptr<backend> store);
    virtual ~svc_manager_base();
    virtual std::string status();
    virtual svc_status initiate(const std::string& queue,const Json& job, job_id& id);
    virtual svc_status take(const std::string& queue, Json& job);
    virtual svc_status ack(const std::string& queue, const job_id& id);
    virtual svc_status done(const std::string& queue, const job_id& id);
  };



  class svc_manager:public svc_manager_base {
  protected:
    boost::asio::io_service& timer_io;
    int r_milli;
    boost::asio::deadline_timer maintenance_timer;
  public:
    svc_manager(const Json& config
		,boost::asio::io_service& timer_io
		,std::shared_ptr<backend> store);
    virtual ~svc_manager();
    virtual void maintain();
  };
}

#endif
