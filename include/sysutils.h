#ifndef __SYSUTILS__
#define __SYSUTILS__

namespace qurlsvc {

  void daemonize(const std::string dir = "/",
                 const std::string stdinfile = "/dev/null",
                 const std::string stdoutfile = "/dev/null",
                 const std::string stderrfile = "/dev/null");
  void daemonize(const std::string &searchsvc_home
		 ,bool daemonize_flag
		 ,const std::string& log_file);
  
  bool lock_file(int fd,bool wait);
  void unlock_file(int fd);
}

#endif
