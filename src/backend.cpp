#include <hiredis/hiredis.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include "common.h"
#include "logutils.h"
#include "backend.h"


namespace qurlsvc {
  class redis_backend:public backend {
  private:
    redisContext* ctx;
    const std::string keyspace;

  public:
    redis_backend(const Json& config_):backend(config_),ctx(0),keyspace("qurl") {
      open();
    }

    virtual ~redis_backend(){
      close();
    }

    virtual svc_status initiate(const std::string& queue,const Json &job, job_id& id) {
      auto rc = svc_status::NOK;
      auto now_utc = boost::posix_time::second_clock::universal_time();
      auto now = utc_now("%Y/%m/%d %H:%M:%S",now_utc);

      job_id tmp_id;
      rc = next_id(tmp_id);
      if(rc != svc_status::OK) {
	return rc;
      }

      Json item = Json::object({
	  {"id",tmp_id}
	  ,{"created_at", now}
	  ,{"job_status", (int)job_status::OPEN}
	  ,{"payload",job}
	});

      auto exec_cmd = "EXEC";
      auto set_cmd = "SET "+queue+":%s"+" "+"%s";
      auto lpush_cmd = "LPUSH "+queue+" "+"%s";
      redisReply *reply;

      redisAppendCommand(ctx,exec_cmd);
      redisAppendCommand(ctx,set_cmd.c_str(),tmp_id.c_str(),item.dump().c_str());
      redisAppendCommand(ctx,lpush_cmd.c_str(),tmp_id.c_str());

      for(size_t pz=0; pz < 3; pz++) {
	if(redisGetReply(ctx,(void**)&reply) != REDIS_OK && rc==svc_status::OK){
	  rc = svc_status::NOK;
	}
	freeReplyObject(reply);
      }

      if(rc == svc_status::OK) {
	id = tmp_id;
      }

      return rc;
    }

    virtual svc_status take(const std::string& queue, Json &job,const uint32_t timeout=0) {
      auto rc = svc_status::NOK;
      auto lpop_cmd = "LPOP %s";
      std::string id;
      redisReply *reply;

      //pop the job id
      reply = (redisReply*)redisCommand(ctx,lpop_cmd,queue.c_str());
      if(reply && reply->type != REDIS_REPLY_ERROR && reply->str) {
	try {
	  id = reply->str;
	  rc = id.empty() ? svc_status::NOK: svc_status::OK;
	}catch(const boost::bad_lexical_cast &le){
	  Log::error("redis-backend - lexical cast error when take");
	}
	freeReplyObject(reply);
      }else {
	Log::error("redis-backend - could not take");
      }

      if(rc==svc_status::NOK) {
	Log::error("empty job or error take");
	return rc;
      }

      Json tmp_job;
      rc = fetch(queue,id,tmp_job);
      if(rc==svc_status::NOK) {
	Log::error("empty job or error fetch");
	return rc;
      }

      tmp_job = Json::object({
	  {"id",tmp_job["id"]}
	  ,{"job_status",(int)job_status::TAKEN}
	  ,{"created_at",tmp_job["created_at"]}
	  ,{"payload",tmp_job["payload"]}
	});
      rc = update(queue,id,tmp_job);
      if(rc == svc_status::OK) {
	job = tmp_job;
      }
      return rc;
    }

    svc_status ack(const std::string& queue, const job_id& id) {
      Json tmp_job;
      auto rc = fetch(queue,id,tmp_job);
      if(rc != svc_status::OK) {
	return rc;
      }


      if(tmp_job["job_status"].int_value() == (int)job_status::TAKEN) {
	tmp_job = Json::object({
	    {"id",tmp_job["id"]}
	    ,{"job_status",(int)job_status::ACKD}
	    ,{"created_at",tmp_job["created_at"]}
	    ,{"payload",tmp_job["payload"]}
	  });
	rc = update(queue,id,tmp_job);
      }else {
	rc = svc_status::NOK;
      }

      return rc;
    }

    svc_status done(const std::string& queue, const job_id& id) {
      Json tmp_job;
      auto rc = fetch(queue,id,tmp_job);
      if(rc != svc_status::OK) {
	return rc;
      }

      if(tmp_job["job_status"].int_value() == (int)job_status::ACKD) {
	rc = drop(queue,id);
      }

      return rc;
    }

  private:

    svc_status fetch(const std::string& queue, const job_id& id, Json& job) {
      auto rc=svc_status::NOK;
      auto get_cmd = "GET %s:%s";
      redisReply *reply;

      reply = (redisReply*)redisCommand(ctx,get_cmd,queue.c_str(),id.c_str());
      if(reply && reply->type != REDIS_REPLY_ERROR) {
	try {
	  std::string err;
	  job = Json::parse(reply->str,err);
	  rc = err.empty() ? svc_status::OK: svc_status::NOK;
	}catch(const boost::bad_lexical_cast &le){
	  Log::error("redis-backend - lexical cast error when fetch");
	}
	freeReplyObject(reply);
      }else {
	Log::error("redis-backend - could not fetch");
      }
      return rc;
    }


    svc_status update(const std::string& queue, const job_id& id, const Json& job) {
      auto rc=svc_status::NOK;
      auto set_cmd = "SET %s:%s %s";
      redisReply *reply;

      reply = (redisReply*)redisCommand(ctx,set_cmd,queue.c_str(),id.c_str(),job.dump().c_str());
      if(reply && reply->type != REDIS_REPLY_ERROR) {
	rc = svc_status::OK;
	freeReplyObject(reply);
      }else {
	Log::error("redis-backend - could not update");
      }
      return rc;
    }

    svc_status drop(const std::string& queue, const job_id& id){
      auto rc=svc_status::NOK;
      auto get_cmd = "DEL %s:%s";
      redisReply *reply;

      reply = (redisReply*)redisCommand(ctx,get_cmd,queue.c_str(),id.c_str());
      if(reply && reply->type != REDIS_REPLY_ERROR) {
	rc = svc_status::OK;
	freeReplyObject(reply);
      }else {
	Log::error("redis-backend - could not drop");
      }
      return rc;
    }

    svc_status next_id(job_id &id) {
      auto rc = svc_status::NOK;
      
      static const std::string __incr_cmd__ = std::string("INCR ")+keyspace+std::string(":job:seq");
      
      redisReply *reply;
      reply = (redisReply*)redisCommand(ctx,__incr_cmd__.c_str());
      if(reply && reply->type != REDIS_REPLY_ERROR) {
	try {
	  id = std::to_string(reply->integer);
	  rc = id.empty() ? svc_status::NOK : svc_status::OK;
	}catch(const boost::bad_lexical_cast &le){
	  Log::error("redis-backend - lexical cast error when increment seq");
	}
	freeReplyObject(reply);
      }else {
	Log::error("redis-backend - could not increment seq");
      }
      return rc;
    }

    void close() {
      if(ctx) {
	redisFree(ctx);
      }
    }

    void open() {
      struct timeval timeout = {config["redis"]["connect_timeout_sec"].int_value(), 500000}; // 1.5 seconds
      redisContext *c = redisConnectWithTimeout(config["redis"]["host"].string_value().c_str()
						,config["redis"]["port"].int_value()
						,timeout);
      if(c == NULL || c->err){
	if(c){
	  throw std::string("redis-backend - connection error: "+std::string(c->errstr));
	}else{
	  throw std::string("redis-backend - connection error: can't allocate context");
	}
      }else {
	ctx = c;
      }
    }
  };

}

namespace qurlsvc {
  std::shared_ptr<backend> backend::make(const Json& config){
    return std::make_shared<redis_backend>(config);
  }
}
