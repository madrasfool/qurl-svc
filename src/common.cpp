#include <boost/thread/mutex.hpp>
#include <boost/timer/timer.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "common.h"

using namespace boost;

namespace qurlsvc {
  static boost::mutex _time_utils_guard;

  std::string utc_now(const std::string& fmt,boost::posix_time::ptime now_utc) {
    boost::mutex::scoped_lock lock(_time_utils_guard);
    static std::stringstream ss;
    static boost::posix_time::time_facet *facet = NULL;
    ss.clear();ss.str("");
    if(facet == NULL) {
      facet = new boost::posix_time::time_facet(1);
      ss.imbue(std::locale(std::locale(), facet));
    }
    facet->format(fmt.c_str());
    ss << now_utc;
    return ss.str();
  }


  std::string utc_at(const std::string& fmt,const uint32_t days) {
    const boost::gregorian::date at = boost::gregorian::day_clock::universal_day() - boost::gregorian::days(days);
    const boost::posix_time::ptime tm(at);

    boost::mutex::scoped_lock lock(_time_utils_guard);
    static std::stringstream ss;
    static boost::posix_time::time_facet *facet = NULL;
    ss.clear();ss.str("");
    if(facet == NULL) {
      facet = new boost::posix_time::time_facet(1);
      ss.imbue(std::locale(std::locale(), facet));
    }
    facet->format(fmt.c_str());
    ss << tm;
    return ss.str();
  }
  
}
