#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/resource.h>
#include <unistd.h>

#include <fstream>
#include <stdexcept>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

#include "logutils.h"
#include "common.h"

using namespace std;
using namespace boost;

namespace qurlsvc {
  extern int __logdebug__;
  extern int __loginfo__;
  extern int __logwarn__;
  extern int __logerror__;
  extern int __logmetric__;
  extern std::string __logfilename__;

  static ofstream __logfile__;
  static boost::mutex __log_guard;

  static void __openlog__() {
    if(!__logfile__.is_open()) {
      boost::mutex::scoped_lock lock(__log_guard);
      if(boost::filesystem::exists(__logfilename__)) {
          boost::system::error_code ec;
          try {

            const posix_time::ptime now_utc = posix_time::second_clock::universal_time();
            const std::string now_str=utc_now("%Y-%m-%d.%H",now_utc);
            const std::string rfile = __logfilename__+"."+now_str;
            if(boost::filesystem::exists(rfile)) {
              boost::filesystem::remove(rfile,ec);
            }
            boost::filesystem::rename(__logfilename__,rfile,ec);
          }catch (std::runtime_error &e) {
          }catch (std::exception &e) {
          }catch(...){
          }
      }
      __logfile__.open(__logfilename__.c_str(), ios_base::out | ios_base::trunc);
      __logfile__.imbue(locale(__logfile__.getloc(), new posix_time::time_facet()));
    }
  }

  void Log::close() {
    if(__logfile__.is_open()) {
      __logfile__.close();
    }
  }

  void Log::error(const std::string &msg) {
    if(__logerror__) {
      __openlog__();
      boost::mutex::scoped_lock lock(__log_guard);
      __logfile__ << posix_time::second_clock::local_time() << "::" << "ERROR::" << msg << endl;
    }
  }

  void Log::warn(const std::string &msg){
    if(__logwarn__) {
      __openlog__();
      boost::mutex::scoped_lock lock(__log_guard);
      __logfile__ << posix_time::second_clock::local_time() << "::" << "WARN::" << msg << endl;
    }
  }

  void Log::debug(const std::string &msg) {
    if(__logdebug__) {
      __openlog__();
      boost::mutex::scoped_lock lock(__log_guard);
      __logfile__ << posix_time::second_clock::local_time() << "::" << "DEBUG::" << msg << endl;
    }
  }

  void Log::info(const std::string &msg) {
    if(__loginfo__) {
      __openlog__();
      boost::mutex::scoped_lock lock(__log_guard);
      __logfile__ << posix_time::second_clock::local_time() << "::" << "INFO::" << msg << endl;
    }
  }

  void Log::admin(const std::string &msg) {
    __openlog__();
    boost::mutex::scoped_lock lock(__log_guard);
    __logfile__ << posix_time::second_clock::local_time() << "::" << "ADMIN::" << msg << endl;
  }
};
