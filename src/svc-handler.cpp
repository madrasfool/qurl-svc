#include <fstream>
#include <boost/filesystem.hpp>
#include <onion/onion.hpp>
#include <onion/request.hpp>
#include <onion/response.hpp>

#include <json11/json11.hpp>


#include "common.h"
#include "logutils.h"
#include "svc-handler.h"

namespace qurlsvc {

  onion_connection_status svc_handler::status(Onion::Request &req, Onion::Response &res) {
    if(!is_get(req))return OCS_NOT_PROCESSED;

    res.setHeader("Content-Type","text/plain");
    res << mgr->status();

    return OCS_PROCESSED;
  }

  onion_connection_status svc_handler::jobs(Onion::Request &req, Onion::Response &res) {
    dump_cors_headers(res);
    res.setHeader("Content-Type","application/json");

    if(is_options(req)){
      res << "{}";
      return OCS_PROCESSED;
    }else if(is_get(req)) {
      return take(req,res);
    }else if(is_post(req)) {
      return initiate(req,res);
    }else if(is_put(req)) {
      return ack(req,res);
    }else if(is_delete(req)) {
      return done(req,res);
    }
      
    return OCS_NOT_PROCESSED;
  }

  onion_connection_status svc_handler::initiate(Onion::Request &req, Onion::Response &res) {
    std::string queue;
    Json job;
    job_id id;
    Json result;
    svc_status rc = json_payload(req,job);
    if(rc != svc_status::OK) {
      Log::error("not processsed");
      return OCS_NOT_PROCESSED;
    }
    
    queue = job["queue"].string_value();
    if(queue.empty()) {
      return OCS_NOT_PROCESSED;
    }

    if(mgr->initiate(queue,job,id) == svc_status::OK) {
      result = Json::object({
	  {"id", id}
	  ,{"status", (int)svc_status::OK}
	});
    }else {
      result = Json::object({
	  {"status", (int)svc_status::NOK}
	});
    }
    res << result.dump();
    return OCS_PROCESSED;
  }

  onion_connection_status svc_handler::take(Onion::Request &req, Onion::Response &res) {
    auto queue = req.query("q","");
    if(queue.empty()) {
      return OCS_NOT_PROCESSED;
    }

    Json job;
    auto rc = mgr->take(queue,job);

    Json result;
    if(rc == svc_status::OK) {
      result = Json::object({
	  {"id", job["id"]}
	  ,{"created_at", job["created_at"]}
	  ,{"job_status",job["job_status"]}
	  ,{"payload",job["payload"]}
	  ,{"status", (int)svc_status::OK}
	});
    }else{
      result = Json::object({
	  {"status", (int)svc_status::NOK}
	});
    }
    res << result.dump();
    return OCS_PROCESSED;
  }

  onion_connection_status svc_handler::done(Onion::Request &req, Onion::Response &res) {
    const std::string q = req.query("q","");
    const job_id id = req.query("id","");
    if(q.empty() || id.empty()) {
      return OCS_NOT_PROCESSED;
    }
    svc_status rc = mgr->done(q,id);
    Json result = Json::object({
	{"status", (int)rc}
      });
    res << result.dump();
    return OCS_PROCESSED;
  }

  onion_connection_status svc_handler::ack(Onion::Request &req, Onion::Response &res) {
    Json job;
    svc_status rc = json_payload(req,job);

    if(rc != svc_status::OK) {
      Log::error("not processsed");
      return OCS_NOT_PROCESSED;
    }
    
    const std::string queue = job["queue"].string_value();
    const job_id id = job["id"].string_value();
    if(id.empty() || queue.empty()) {
      return OCS_NOT_PROCESSED;
    }
    rc = mgr->ack(queue,id);
    Json result = Json::object({
	{"status", (int)rc}
      });
    res << result.dump();
    return OCS_PROCESSED;
  }

  // ====== protected stuff =====================================


  void svc_handler::dump_cors_headers(Onion::Response &res) {
    res.setHeader("Access-Control-Allow-Origin","*"); 
    res.setHeader("Access-Control-Allow-Method","OPTIONS, GET, POST, PUT");
    res.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Content-Type","application/json");
  }

  bool svc_handler::is_options(Onion::Request& req) {
    return ((onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_OPTIONS) || ((onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_HEAD) ;
  }

  bool svc_handler::is_get(Onion::Request& req) {
    return (onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_GET ;
  }

  bool svc_handler::is_post(Onion::Request& req) {
    return ( ((onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_POST) );
  }

  bool svc_handler::is_put(Onion::Request& req) {
    return ( ((onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_PUT) );
  }

  bool svc_handler::is_delete(Onion::Request& req) {
    return (onion_request_get_flags(req.c_handler())&OR_METHODS) == OR_DELETE;
  }


  svc_status svc_handler::json_payload(Onion::Request &req, Json& payload) {
    svc_status result = svc_status::NOK;
    onion_request* c_req = req.c_handler();
    const onion_block *dreq=onion_request_get_data(c_req);
    if(dreq) {
      std::string err;
      std::string data = onion_block_data(dreq);
      if(boost::filesystem::exists(data)) {
	std::ifstream in(data.c_str());
	std::stringstream sstr;
	sstr << in.rdbuf();
	payload = Json::parse(sstr.str(),err);
      }else {
	payload = Json::parse(data,err);
      }

      if(!err.empty()) {
	Log::error("json parse error for request - "+req.path()+",data="+std::string(onion_block_data(dreq)));
      }
      result = err.empty() ? svc_status::OK : svc_status::NOK;
    }else {
      Log::error("empty payload for re  quest - "+req.path());
    }
    return result;
  }

}
