#include <curl/curl.h>

#include <fstream>
#include <thread>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/timer.hpp>

#include <onion/onion.hpp>
#include <onion/response.hpp>
#include <onion/url.hpp>

#include "common.h"
#include "logutils.h"
#include "sysutils.h"
#include "backend.h"
#include "svc-manager.h"
#include "svc-handler.h"

namespace qurlsvc {
  int __logdebug__ = 0;
  int __loginfo__ = 0;
  int __logwarn__ = 1;
  int __logerror__ = 1;

  std::string __logfilename__;


  boost::asio::io_service timer_io;
  boost::shared_ptr<Onion::Onion> o_server;

  void cleanup_server() {
    qurlsvc::Log::admin("qurlsvc::signal::stop signal recieved ...");
    qurlsvc::timer_io.stop();
    qurlsvc::Log::admin("qurlsvc::signal::processed stop signal ...");
  }
}


void print_config(const Json& config) {
  std::string name,value;

  std::cout << std::endl;
  std::cout << "configuration used" << std::endl;
  std::cout << "-------------------" << std::endl;
  std::cout << config.dump();
  std::cout << std::endl;
}

void print_help() {
  std::cerr << "qurl-svc    --config <configuration file> " <<std::endl;
  std::cerr << "            --help" << std::endl;
  exit(0);
}


void load_config(int argc,char **argv,Json& config) {
  std::string config_file;
  std::string opt;
  for(int i = 0; i < argc; i++) {
    opt = argv[i];
    if(opt == "--config") {
      config_file = argv[i+1];
    }else if(opt == "--help") {
      print_help();
    }
  }

  std::ifstream in(config_file.c_str());
  std::stringstream sstr;
  std::string err;
  sstr << in.rdbuf();
  config = Json::parse(sstr.str(),err);
  if(!err.empty()) {
    std::cerr << "error loading configuration file '" << config_file << "' - " << err << std::endl;
    exit(1);
  }
}


void validate_config(Json& config) {
  bool has_error = false;
  std::string iname=config["instance"].string_value();
  if(iname.empty()) {
    std::cerr << "error: instance name is empty or nil" << std::endl;
    has_error = true;
  }


  if(!boost::filesystem::is_directory(config["home_dir"].string_value())) {
    std::cerr << "error: invalid home dir '"<<config["home_dir"].string_value()<<"'"<<std::endl;
    has_error = true;
  }

  if(!boost::filesystem::is_directory(config["log_dir"].string_value())) {
    std::cerr << "error: log dir '"<<config["log_dir"].string_value()<<"' doesn't exist"<<std::endl;
    has_error = true;
  }
  
  if(has_error) {
    exit(1);
  }
}

void start_http_listen() {
  qurlsvc::o_server->listen();
}

void setup_log_levels(const Json& config) {
  if(config["log_level"].string_value() == "info") {
    qurlsvc::__logdebug__ = 0;
    qurlsvc::__loginfo__ = 1;
  }else if(config["log_level"].string_value() == "debug") {
    qurlsvc::__logdebug__ = 1;
    qurlsvc::__loginfo__ = 1;
  }else {
    qurlsvc::__logdebug__ = 0;
    qurlsvc::__loginfo__ = 0;
  }
}

int setup_pid_file(const Json& config) {
  const std::string pid_file = config["pid_file"].string_value();
  if(boost::filesystem::exists(pid_file)) {
    std::cerr << "pid file alread exist '"+pid_file << std::endl;
    exit(1);
  }

  int pid_fd = ::open(pid_file.c_str(), O_CREAT | O_WRONLY, S_IREAD | S_IWRITE);
  if(pid_fd < 0) {
    std::cerr << "could not open pid file '"+pid_file << std::endl;
    exit(1);
  }

  if(!qurlsvc::lock_file(pid_fd,false)) {
    std::cerr << "could not lock pid file '"+pid_file << std::endl;
    exit(1);
  }

  char s_pid[16];
  snprintf(s_pid,sizeof(s_pid), "%d\n", (int)getpid());
  int pid_len = strlen(s_pid);

  ::lseek(pid_fd, 0, SEEK_SET);
  int write_len = ::write(pid_fd,s_pid,pid_len);
  if(write_len!=pid_len){
    std::cerr << "failed to write to pid file '" << pid_file << ",write len = " << write_len <<std::endl;
    exit(1);
  }
  if (::ftruncate(pid_fd,pid_len)) {
    std::cerr << "failed to truncate pid file '" << pid_file << std::endl;
    exit(1);
  }

  return pid_fd;
}

int main(int argc, char **argv){

  Json config;
  bool has_error = false;
  std::srand(std::time(0));
  
  load_config(argc,argv,config);
  validate_config(config);
  setup_log_levels(config);

  bool is_daemonize = config["daemonize"].bool_value();
  if(is_daemonize) {
    print_config(config);
  }
  const std::string log_file = config["log_dir"].string_value()+"/"+config["log_file"].string_value();
  qurlsvc::daemonize(config["home_dir"].string_value(), is_daemonize, log_file);
  qurlsvc::Log::admin("qurlsvc::homedir::"+config["home_dir"].string_value());

  int pid_fd = setup_pid_file(config);

  try {
    qurlsvc::Log::admin("qurlsvc::initialize ...");

    curl_global_init(CURL_GLOBAL_ALL);

    std::shared_ptr<qurlsvc::backend> store = qurlsvc::backend::make(config);
    std::shared_ptr<qurlsvc::svc_manager_base> mgr = std::make_shared<qurlsvc::svc_manager>(config,qurlsvc::timer_io,store);
    qurlsvc::svc_handler handler(config,mgr);

    //setup onion
    qurlsvc::o_server.reset(new Onion::Onion(O_POLL));
    qurlsvc::o_server->setPort(config["port"].int_value());
    onion_set_max_threads(qurlsvc::o_server->c_handler(), config["max_threads"].int_value());
    Onion::Url root(*qurlsvc::o_server);
    
    root.add("status",&handler,&qurlsvc::svc_handler::status);
    
    //GET=take, POST=create, PUT=ack, DELETE=done
    root.add("jobs",&handler,&qurlsvc::svc_handler::jobs);
    
    boost::thread listen_thread(start_http_listen); 
    
    qurlsvc::Log::admin("qurlsvc::listening at http://localhost:"
			+std::to_string(config["port"].int_value()));

    qurlsvc::timer_io.run();
  }catch (std::runtime_error &e) {
    qurlsvc::Log::error(e.what());
    has_error = true;
  }catch(std::exception& e) {
    qurlsvc::Log::error(e.what());
    has_error = true;
  }

  ::close(pid_fd);
  qurlsvc::unlock_file(pid_fd);
  ::unlink(config["pid_file"].string_value().c_str());

  qurlsvc::Log::admin("qurlsvc::exit");
  return (has_error ? -1 : 0);
}
