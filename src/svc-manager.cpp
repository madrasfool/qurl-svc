#include <boost/bind.hpp>
#include "common.h"
#include "logutils.h"
#include "svc-manager.h"


namespace qurlsvc {

  // ===== svc_manager_base =======================================

  svc_manager_base::svc_manager_base(const Json& config_
				     ,std::shared_ptr<backend> storage):config(config_)
								       ,store(storage) {
  }

  svc_manager_base::~svc_manager_base() {
  }


  std::string svc_manager_base::status() {
    return "ok";
  }

  svc_status svc_manager_base::initiate(const std::string& queue,const Json& job, job_id& id) {
    return store->initiate(queue,job,id);
  }

  svc_status svc_manager_base::take(const std::string& queue, Json& job) {
    return store->take(queue,job);
  }

  svc_status svc_manager_base::ack(const std::string& queue, const job_id& id) {
    return store->ack(queue,id);
  }

  svc_status svc_manager_base::done(const std::string& queue, const job_id& id) {
    return store->done(queue,id);
  }


  // ==== derived with timer/maintain ======================

  svc_manager::svc_manager(const Json& config_
			   ,boost::asio::io_service& timer_io_
			   ,std::shared_ptr<backend> storage):
    svc_manager_base(config_,storage)
    ,timer_io(timer_io_)
    ,r_milli(config["maint_timer_milli"].int_value())
    ,maintenance_timer(timer_io, boost::posix_time::millisec(r_milli)) {
    maintenance_timer.async_wait(boost::bind(&svc_manager::maintain, this));
  }

  svc_manager::~svc_manager() {
  }

  void svc_manager::maintain() {
    if(!timer_io.stopped()) {
      maintenance_timer.expires_from_now(boost::posix_time::millisec(r_milli));
      maintenance_timer.async_wait(boost::bind(&svc_manager::maintain, this));
    }
  }
}
