#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <netinet/in.h>
#include <sys/resource.h>
#include <unistd.h>
#include <fstream>
#include <stdexcept>

#include "sysutils.h"
#include "logutils.h"

/**
 *  - daemonize was taken from here http://blog2.emptycrate.com/content/making-linux-daemon
 *  - lock/unlock was swiped from sphinx code
 **/

namespace qurlsvc {

  extern std::string __logfilename__;

  extern void cleanup_server();

  extern "C" void signal_handler(int sig) {
    if(sig==SIGHUP) {
      Log::info("reopen log file");
      Log::close();
    }else if(sig==SIGTERM || sig==SIGKILL) {
      cleanup_server();
    }
  }

  bool lock_file( int iFile, bool bWait ) {
    struct flock tLock;
    tLock.l_type = F_WRLCK;
    tLock.l_whence = SEEK_SET;
    tLock.l_start = 0;
    tLock.l_len = 0;
    int cmd = bWait ? F_SETLKW : F_SETLK;
    return (fcntl(iFile, cmd, &tLock)!=-1);
  }
  
  
  void unlock_file ( int iFile ) {
    struct flock tLock;
    tLock.l_type = F_UNLCK;
    tLock.l_whence = SEEK_SET;
    tLock.l_start = 0;
    tLock.l_len = 0;
    fcntl(iFile, F_SETLK, &tLock);
  }

  void daemonize(const std::string dir,
                 const std::string stdinfile,
                 const std::string stdoutfile,
                 const std::string stderrfile) {

    ::umask(0);

    rlimit rl;
    if(::getrlimit(RLIMIT_NOFILE, &rl) < 0) {
      //can't get file limit
      throw std::runtime_error(strerror(errno));
    }

    pid_t pid;
    if((pid = ::fork()) < 0) {
      //Cannot fork!
      throw std::runtime_error(strerror(errno));
    } else if(pid != 0) {
      //parent
      ::exit(0);
    }

    ::setsid();

    if(!dir.empty() && ::chdir(dir.c_str()) < 0) {
      // Oops we couldn't chdir to the new directory
      throw std::runtime_error(strerror(errno));
    }

    if(rl.rlim_max == RLIM_INFINITY) {
      rl.rlim_max = 1024;
    }

    // Close all open file descriptors
    for(unsigned int i = 0; i < rl.rlim_max; i++) {
      ::close(i);
    }

    int fd0 = ::open(stdinfile.c_str(), O_RDONLY);
    int fd1 = ::open(stdoutfile.c_str(),
                     O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR);
    int fd2 = ::open(stderrfile.c_str(),
                     O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR);

    if(fd0 != STDIN_FILENO || fd1 != STDOUT_FILENO || fd2 != STDERR_FILENO) {
      //Unexpected file descriptors
      throw std::runtime_error("new standard file descriptors were not opened as expected");
    }
  }

  void daemonize(const std::string &svc_home, bool daemonize_flag, const std::string& log_file) {
    if(daemonize_flag) {
      daemonize(svc_home);
    }

    __logfilename__ = log_file;

    signal(SIGCHLD,SIG_IGN);
    signal(SIGTSTP,SIG_IGN);
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);

    signal(SIGHUP,signal_handler);
    signal(SIGTERM,signal_handler);
    signal(SIGKILL,signal_handler);
  }
}
