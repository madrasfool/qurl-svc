#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE LIFECYCLE
#include <boost/test/unit_test.hpp>

#include <fstream>
#include <boost/filesystem.hpp>

#include "common.h"
#include "logutils.h"
#include "sysutils.h"
#include "backend.h"
#include "svc-manager.h"


namespace qurlsvc {
  int __logdebug__ = 1;
  int __loginfo__ = 1;
  int __logwarn__ = 1;
  int __logerror__ = 1;

  std::string __logfilename__;
  void cleanup_server() {
  }
}


void load_config(const std::string& path, Json& config) {
  std::ifstream in(path.c_str());
  std::stringstream sstr;
  std::string err;
  sstr << in.rdbuf();
  config = Json::parse(sstr.str(),err);
  if(!err.empty()) {
    std::cerr << "error loading configuration file '" << path << "' - " << err << std::endl;
    exit(1);
  }
}

BOOST_AUTO_TEST_CASE(lifecycle_simple) {

  qurlsvc::__logfilename__ = "log/test.log";
  Json config;
  load_config("config/qurl-svc.json",config);

  std::shared_ptr<qurlsvc::backend> store = qurlsvc::backend::make(config);
  std::shared_ptr<qurlsvc::svc_manager_base> mgr = std::make_shared<qurlsvc::svc_manager_base>(config,store);

  std::string err;
  Json job;
  qurlsvc::job_id id;
  std::string queue = "qurl:test";
  
  job = Json::object({
      {"queue", queue}
    });

  qurlsvc::svc_status rc = mgr->initiate(queue,job,id);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::OK),"initiate job ...");
  BOOST_CHECK_MESSAGE(!id.empty(),"not empty id ...");

  rc = mgr->take(queue,job);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::OK),"take job ...");
  BOOST_CHECK_MESSAGE((job["job_status"].int_value() == (int)qurlsvc::job_status::TAKEN),"take job status ...");
  rc = mgr->ack(queue,id);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::OK),"ack job ...");

  rc = mgr->ack(queue,id);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::NOK),"ack job in wrong state ...");

  rc = mgr->done(queue,id);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::OK),"done job ...");

  rc = mgr->done(queue,id);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::NOK),"done job not exist ...");

  job = Json::object({});
  rc = mgr->take(queue,job);
  BOOST_CHECK_MESSAGE((rc == qurlsvc::svc_status::NOK),"take job empty ...");
  BOOST_CHECK_MESSAGE((job["id"].is_null()),"take job empty id must be nil...");
}

